<?php

class crud // la class des operations avec la base de données.
{
	private $db;
	
	function __construct($DB_con)
	{
		$this->db = $DB_con;
	}
	
	public function create($login,$pwd,$fname,$lname,$email) // methode d'insertion des données.
	{
		try
		{
			$stmt = $this->db->prepare(
				"INSERT INTO user(login,pwd,nom,prenom,mail) 
						VALUES(:login,:pwd,:fname, :lname, :mail)");
			// affectations des valeurs :
			$stmt->bindparam(":login",$login);
			$stmt->bindparam(":pwd",$pwd);
			$stmt->bindparam(":fname",$fname);
			$stmt->bindparam(":lname",$lname);
			$stmt->bindparam(":mail",$email);
			// execution de la reqeute :
			return $stmt->execute();
		}
		catch(PDOException $e) 
		{					   
			echo $e->getMessage();	
			return false;
		}	
	}

			
	public function dataview($query) 
	{
		$stmt = $this->db->prepare($query);
		$stmt->execute(); // exectuion de la requete	
		if($stmt->rowCount() > 0) 
		{	
			while($row=$stmt->fetch(PDO::FETCH_ASSOC))  
			{									       
				?>
                <tr>
                	<td><?php print($row['id']); ?></td> 
					<td><?php print($row['login']); ?></td>
					<td><?php print($row['pwd']); ?></td>
					<td><?php print($row['nom']); ?></td>
                	<td><?php print($row['prenom']); ?></td>
                	<td><?php print($row['mail']); ?></td>
					<td><a href="generate_pdf.php?nom=<?php echo $row['nom']; ?>&prenom=<?php echo $row['prenom']; ?>">Générer PDF</a></td>

                </tr>
                <?php
			}
		}
		else 
		{
			?>
            <tr>
            <td>Acune utilisateur...</td><!--on affiche la table vide-->
            </tr>
            <?php
		}
	}	
}
?>