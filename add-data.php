<?php
/*
  Author : Mohamed Aimane Skhairi
  Email : skhairimedaimane@gmail.com
  Repo : https://github.com/medaimane/crud-php-pdo-bootstrap-mysql
*/

include_once 'dbconfig.php'; // include l'instance de la class crud.
if(isset($_POST['btn-save'])){ // test sur le bouton. 
	$fname = $_POST['nom']; // affectation des valeur evoier par la method post.
	$lname = $_POST['prenom'];
	$email = $_POST['mail'];
	$login = $_POST['login'];
    $pwd = $_POST['pwd'];
	if($crud->create($login,$pwd,$fname,$lname,$email)){ // test sur l'execution du requete, 
        header("Location: add-data.php?inserted");    // si tout passe bien returne true, et on recharge la page
    }else{                                            // mais avec "inserted" comme paramétre. 
		header("Location: add-data.php?failure");     // sinon on recharge la page avec "failure" comme paramétre.
	}}
?>
<?php include_once 'header.php'; ?>
<?php
if(isset($_GET['inserted'])){
	?>
    <div class="container">
	   <div class="alert alert-info">
        Insertion avec success 
	   </div>
	</div>
    <?php
}else if(isset($_GET['failure'])){ 
	?>
    <div class="container">
	   <div class="alert alert-warning">
        Erreur d'insertion
	   </div>
	</div>
    <?php
    }
?>

<div class="container">
	<form method='post'>
    <table class='table table-bordered'>
    <tr>
            <td>Login</td><td><input type='text' name='login' class='form-control' required></td>
        </tr>
        <tr>
            <td>password</td><td><input type='text' name='pwd' class='form-control' required></td>
        </tr>
        <tr>
            <td>Nom</td><td><input type='text' name='nom' class='form-control' required></td>
        </tr>
        <tr>
            <td>Prénom</td><td><input type='text' name='prenom' class='form-control' required></td>
        </tr>
        <tr>
            <td>E - mail</td><td><input type='text' name='mail' class='form-control' required></td>
        </tr>
        <tr>
            <td colspan="2">
            <!--btn-save : button de confirmation-->
            <button type="submit" class="btn btn-primary" name="btn-save">
    		<span class="glyphicon glyphicon-plus"></span> Crée l'utilisateur</button>
            <!--lien de retour vers l'index-->  
            <a href="index.php" class="btn btn-large btn-success" style="float: right;">
            <i class="glyphicon glyphicon-backward"></i> &nbsp; Retourner vers l'index</a>
            </td>
        </tr>
    </table><!--fin du tableau-->
</form><!--fin de form-->
</div>
<?php include_once 'footer.php'; ?>