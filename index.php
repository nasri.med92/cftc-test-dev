<?php include_once 'dbconfig.php'; ?> 
<?php include_once 'header.php'; ?> 

<div class="container">
    <!--lien vers la page d'ajoute d'utilisateur-->
    <a href="add-data.php" class="btn btn-large btn-info">
        <i class="glyphicon glyphicon-plus"></i> &nbsp; Ajouter un utilisateur
    </a>
</div>
<br />
<div class="container"> 
    <!--creation du tableau-->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.css" />
	<table class='table table-bordered table-responsive' id="tblUser"> 
    <thead>
        <tr>
            <th>ID</th>
            <th>Login</th>
            <th>PWD</th>
            <th>Nom </th>
            <th>Prénom</th>
            <th>E - mail</th>
            <th>Action</th>
        </tr>
</thead>
<tbody>
        <?php    
		  $crud->dataview("SELECT * FROM user"); // l'appele du méthode d'affichage.
	    ?>
        </tbody>
    </table>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>
<script>
jQuery(document).ready(function($) {
    $('#tblUser').DataTable();
} );
</script>
</div>
<?php include_once 'footer.php'; ?>