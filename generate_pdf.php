<?php
// Inclure les bibliothèques QR Code et PDF
include "phpqrcode/qrlib.php";
require "fpdf/fpdf.php";

if (isset($_GET['nom']) && isset($_GET['prenom'])) {
    $nom = $_GET['nom'];
    $prenom = $_GET['prenom'];

    // Générer le contenu du QR Code
    $qr_content = "Nom: " . $nom . "\nPrénom: " . $prenom;

    // Chemin où le fichier PDF sera généré
    $output_file = "users/" . $prenom . "_" . $nom . ".pdf";

    // Créer un objet PDF
    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->SetFont('Arial', 'B', 16);

    $pdf->Image("cftc-logo.jpg", 10, 10, 30, 30);

    // Générer le QR Code et l'ajouter dans le PDF
    QRcode::png($qr_content, "qrcodes/" . $prenom . "_" . $nom . ".png"); 
    $pdf->Image("qrcodes/" . $prenom . "_" . $nom . ".png", 10, 50, 50, 50);

    // Ajouter le nom et prénom sous l'image du QR Code
    $pdf->SetXY(10, 110);
    $pdf->Cell(0, 10, "Nom: " . $nom, 0, 1, 'C');
    $pdf->Cell(0, 10, "Prénom: " . $prenom, 0, 1, 'C');


    // Sauvegarder le PDF dans un fichier
    $pdf->Output("F", $output_file);

    // Afficher un lien pour télécharger le PDF
    echo "<a href='" . $output_file . "'>Télécharger le PDF</a>";
} else {
    echo "Aucun utilisateur spécifié.";
}
?>
